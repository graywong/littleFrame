﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.Core.Users
{
    [Serializable()]
    public partial class User
    {
        public int UserID
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string UserPassword
        {
            get;
            set;
        }
        public int? RoleID
        {
            get;
            set;
        }
        public int? DeptID
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        public string Fax
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string QQ
        {
            get;
            set;
        }
        public string NickName
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public string RealName
        {
            get;
            set;
        }
        public bool? Sex
        {
            get;
            set;
        }
        public bool? Enabled
        {
            get;
            set;
        }
        public DateTime? LastLoginTime
        {
            get;
            set;
        }
        public int? CreateUserID
        {
            get;
            set;
        }
        public DateTime? CreateDate
        {
            get;
            set;
        }
        public int? ModifyUserID
        {
            get;
            set;
        }
        public DateTime? ModifyDate
        {
            get;
            set;
        }
        public string RecordStatus
        {
            get;
            set;
        }
        public bool? IsDeleted
        {
            get;
            set;
        }
    }
}
