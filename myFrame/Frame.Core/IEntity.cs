﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.Core
{
    public interface IEntity
    {
        // 当前领域实体的全局唯一标识
        long Id { get; set; }
    }
}
