﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.DTO
{
    [Serializable()]
    public class ResultDto<T>
    {
        private const long serialVersionUID = -1L;
        private int _code;
        private string _msg;
        private T _data;

        public int Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public string Msg
        {
            get
            {
                return _msg;
            }
            set
            {
                _msg = value;
            }
        }
        public T Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }

        public ResultDto()
        {
        }

        public ResultDto(int code)
            : this(code, "", default(T))
        {
            
        }

        public ResultDto(int code, T data)
            : this(code, "", data)
        {
        }

        public ResultDto(int code, string msg)
            : this(code, msg, default(T))
        {
        }

        public ResultDto(int code, string msg, T data)
        {
            this._code = code;
            this._msg = msg;
            this._data = data;
        }
    }
}
