﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.DTO.Users
{
    public class GetUserOutput
    {
        public int UserID
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        public string Fax
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string QQ
        {
            get;
            set;
        }
        public string NickName
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public string RealName
        {
            get;
            set;
        }
        public bool? Sex
        {
            get;
            set;
        }
      
        public DateTime? LastLoginTime
        {
            get;
            set;
        }
       
        public DateTime? ModifyDate
        {
            get;
            set;
        }
      
    }
}
