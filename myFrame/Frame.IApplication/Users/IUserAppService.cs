﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frame.DTO;
using Frame.Core.Users;
using Tools.Other;
using Frame.DTO.Users;

namespace Frame.IApplication.Users
{
    public interface IUserAppService
    {
         ResultDto<Frame.DTO.Users.GetUserOutput> getUser(int id);
         User getUser();
         ResultDto<User> getUser2(int id);
    }
}
