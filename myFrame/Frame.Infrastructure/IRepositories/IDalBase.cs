﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Frame.IRepositories
{
    public interface IDalBase<TEntity, TPrimaryKey> 
    {
        //#region 属性
        //string _message = "";
        //string Message
        //{
        //    get { return _message; }
        //    set { _message = value; }
        //}
        //#endregion

         bool Insert(TEntity model, SqlTransaction dbt = null);

         int Update(TEntity info, SqlTransaction dbt = null);

         int Delete(TPrimaryKey id, SqlTransaction dbt = null);

         TEntity GetModel(TPrimaryKey id, SqlTransaction dbt = null);

         DataSet GetModelDs(TPrimaryKey id, SqlTransaction dbt = null);
    }
}
