﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frame.Core.Users;
namespace Frame.IRepositories.Users
{
    public interface IUserProvider : Frame.IRepositories.IDalBase<User, int>
    {
    }
}
