﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Frame.IApplication.Users;
using Frame.DTO;
namespace Frame.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private IUserAppService userAppService;
        public HomeController(IUserAppService userAppService)
        {
            this.userAppService = userAppService;
        }
        public ActionResult Index()
        {
            //ResultDto<Frame.DTO.Users.GetUserOutput> r = userAppService.getUser(1);
            ResultDto<Frame.Core.Users.User> r2 = userAppService.getUser2(1);
            ViewData["test"] = Tools.Other.JsonHelper.ToJson(r2);
            return View();
        }

    }
}
