﻿using Frame.Core;
using Frame.EntityFrame.IEfRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.EntityFrame.EfRepositories
{
    public abstract class EntityFrameworkRepository<TAggregateRoot> : IRepository<TAggregateRoot>
       where TAggregateRoot : class//, IEntity<int>
    {
        private readonly IEntityFrameworkRepositoryContext _efContext;
        protected IEntityFrameworkRepositoryContext EfContext
        {
            get { return this._efContext; }
        }
        protected EntityFrameworkRepository(IRepositoryContext context)
        {
            var efContext = context as IEntityFrameworkRepositoryContext;
            if (efContext != null)
                this._efContext = efContext;
        }

        //public TAggregateRoot GetByKey(int key)
        //{
        //    return _efContext.DbContex.Set<TAggregateRoot>().First(a => a.Id == key);
        //}

        public void Add(TAggregateRoot aggregateRoot)
        {
            // 调用IEntityFrameworkRepositoryContext的RegisterNew方法将实体添加进DbContext.DbSet对象中
            _efContext.DbContex.Set<TAggregateRoot>().Add(aggregateRoot);
        }
    }
}
