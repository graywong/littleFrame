﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frame.EntityFrame;
using Frame.EntityFrame.IEfRepositories;
using Frame.Core.Users;

namespace Frame.EntityFrame.EfRepositories.Users
{
    //如果用entity framework，一般没必要建个DAL层,直接在Application层调用MyDbContext即可
    public class UserEfProvider //无法依赖注入，也没啥意义: MyDbContext
    {
        public User getSomeUser(int id,MyDbContext mdc)
        {
            User u = mdc.User.ToList().Where(w => w.UserID == id).First();
            return u;
        }
    }
}
