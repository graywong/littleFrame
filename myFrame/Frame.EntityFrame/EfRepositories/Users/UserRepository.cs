﻿using Frame.Core.Users;
using Frame.EntityFrame.IEfRepositories;
using Frame.EntityFrame.IEfRepositories.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.EntityFrame.EfRepositories.Users
{
    public class UserRepository : EntityFrameworkRepository<User>, IUserRepository
    {
        public UserRepository(IRepositoryContext context)
            : base(context)
        {
        }

        public IDbSet<User> Users
        {
            get
            {
                return this.EfContext.DbContex.User;
            }
        }
        
    }
}
