﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.EntityFrame.IEfRepositories
{
    public interface IEntityFrameworkRepositoryContext : IRepositoryContext
    {
        #region Properties
        MyDbContext DbContex { get; }
        #endregion
    }
}
