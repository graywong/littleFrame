﻿using Frame.Core.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.EntityFrame.IEfRepositories.Users
{
    public interface IUserRepository : IRepository<User>
    {
        //bool CheckPassword(string userName, string password);
        IDbSet<User> Users
        {
            get;
        }
    }
}
