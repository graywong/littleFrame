﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frame.EntityFrame.IEfRepositories
{
    public interface IRepositoryContext : IUnitOfWork
    {
        Guid Id { get; }
    }
}
