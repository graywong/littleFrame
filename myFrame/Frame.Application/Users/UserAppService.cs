﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Frame.IRepositories.Users;
using Frame.Core.Users;
//using Frame.Infrastructure.Repositories.Users;
using Frame.DTO;
using Tools.Other;
using Frame.DTO.Users;
using Frame.IApplication.Users;
using Frame.EntityFrame.IEfRepositories;
using Frame.EntityFrame;
using Frame.EntityFrame.EfRepositories.Users;
using Frame.EntityFrame.IEfRepositories.Users;
namespace Frame.Application.Users
{
    public class UserAppService :ServiceBase, IUserAppService
    {
        IUserProvider _UserProvider;
        IUserRepository _IUserRepository;
        public UserAppService(IUserProvider userProvider, IUserRepository userRepository)
        {
            _UserProvider = userProvider;
            _IUserRepository = userRepository;
        }
        //public UserAppService():this(new UserProvider())
        //{
            
        //}

        //public User getUser(int id)
        //{
        //    User u = _UserProvider.GetModel(id);
        //    return u;
        //}

        public ResultDto<Frame.DTO.Users.GetUserOutput> getUser(int id)
        {

            DataSet ds = _UserProvider.GetModelDs(id);
            if (ds.dsIsNullOrEmpty())
            {
                return new ResultDto<GetUserOutput>(ResultCode.NOT_FOUND);
            }
            else
            {
                GetUserOutput guot = ds.Tables[0].ToList<GetUserOutput>()[0];
                return new ResultDto<GetUserOutput>(ResultCode.SUCCESS, "", guot);
            }
        }

        //Entity framework的做法(EF不需要自己做Repositories,因为EF本身就是已经做好的Repositories)
        //如果觉得有些代码重复太多，可以做你的EfRepositories
        public User getUser()
        {
            //用这种方式替代依赖注入
            using (var m =  this.NewDB())
            {
                User u = m.User.Where(w => w.UserID == 1).First();
                
                return u;
            }
            
            
        }
        public ResultDto<User> getUser2(int id)
        {
            User user = _IUserRepository.Users.First();
            if (user == null)
            {
                return new ResultDto<User>(ResultCode.NOT_FOUND);
            }
            else
            {

                return new ResultDto<User>(ResultCode.SUCCESS, "", user);
            }
        }


        
    }
}
