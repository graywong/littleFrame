﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frame.EntityFrame.IEfRepositories;
using Frame.EntityFrame;

namespace Frame.Application
{
    public abstract class ServiceBase
    {
        protected MyDbContext NewDB()
        {
            return new MyDbContext();
        }

        //protected void Try(Action action)
        //{
        //    try
        //    {
        //        action();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.TryLog("ServiceBase.Try", ex);
        //    }
        //}
    }
}
