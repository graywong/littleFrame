﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.Core;
using Castle.Windsor;
using Castle.Windsor.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;


namespace Frame.WindorInit
{
    public class SomeInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("Frame.Web") //在哪里找寻接口或类  
               .BasedOn<IController>() //实现IController接口  
               .Configure(c => c.LifestylePerWebRequest()));//每次请求创建一个Controller实例  

            
            container.Register(Classes.FromAssemblyNamed("Frame.Application").InNamespace("Frame.Application",true).WithService.DefaultInterfaces());
            container.Register(Classes.FromAssemblyNamed("Frame.Infrastructure").InNamespace("Frame.Infrastructure.Repositories",true).WithService.DefaultInterfaces());
            container.Register(Classes.FromAssemblyNamed("Frame.EntityFrame").InNamespace("Frame.EntityFrame.EfRepositories", true).WithService.DefaultInterfaces());
            //container.Register(Component.For<Frame.EntityFrame.IEfRepositories.IRepositoryContext>().ImplementedBy<Frame.EntityFrame.EfRepositories.EntityFrameworkRepositoryContext>().LifeStyle.Transient);
            //container.Register(Classes.FromAssemblyNamed("PrintSpace").InNamespace("PrintSpace").WithService.DefaultInterfaces());
        }
    }
}
