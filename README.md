# littleFrame
本框架适合做点中小型项目，不使用ORM，使用castle windsor框架做依赖注入
Core层是领域层，存放实体，以及处理些业务操作；
Infrastructure层是基础设施层，跟数据库打交道的；
Application层是应用层，事务级操作，一个“用例”；
Web展示层